<?php
  

  /* Consultar los datos */
  $id   = $_GET["id"];
  $sql = "SELECT e.id_alumno, e.mensaje, DATE_FORMAT(e.fecha, "%M %d %Y") AS fecha, CONCAT(u.nombre,' ', u.apellido_paterno, ' ', IFNULL(u.apellido_materno,'')) AS nombre FROM exci e LEFT JOIN usuarios u ON u.id = e.id_alumno WHERE e.id_alumno=".$id;
  if (!$resultado = $conn->query($sql)) {
    echo $resultado;
  }
  $data = $resultado->fetch_assoc();
  $nombre = $data['nombre'];
  $mensaje = $data['mensaje'];
  $fecha = $data['fecha'];
  

  $pdf = new tFPDF('P','mm','A4');
  $pdf->AddPage();
  // Add a Unicode font (uses UTF-8)
  $pdf->Image('fondo_exci.jpg' , 0 ,0, 210 , 297,'JPG');

  // Load a UTF-8 string from a file and print it
  // $pdf->AddFont('Plantagenet Cherokee','','plantc.ttf',true);
  $pdf->AddFont('Calibri','','calibri.ttf',true);

  /* Poner la fecha*/
  $pdf->SetFont('Calibri','',10);
  $pdf->MultiCell(160,100, 'Fecha: '.$fecha , $border=0, $align="R", $fill=false);

  /* Poner el nombre*/
  $pdf->SetFont('Calibri','',14);
  $pdf->SetXY(30,90);
  $pdf->Write(10,$nombre);

  /* Poner el mensaje*/
  $pdf->SetFont('Calibri','',12);
  $pdf->SetXY(30,110);
  $pdf->Write(7,$mensaje);

  $pdf->Output();
  
?>