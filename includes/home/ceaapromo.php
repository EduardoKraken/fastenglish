<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Product Card/Page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link id="theme-style" rel="stylesheet" href="includes/cssproduct/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

</head>
<section>
  
  <div class = "card-wrapper">
    <div class = "card">
      <!-- card left -->
      <div class = "product-imgs">
        <div class = "img-display">
          <div class = "img-showcase">
            <img class="img-img" src = "includes/cssproduct/shoes_images/devbook-cover.jpg" alt = "shoe image">
          </div>
        </div>
      </div>

      <!-- card right -->
      <div class = "product-content">
        <h2 class = "product-title">Nivel - FAST ENGLISH</h2>
        <div class = "product-price" id="inicio">
          <p class = "new-price">Precio: <span><b>$1,650.00</b></span></p>
        </div>

        <div class = "product-price" id="promo">
          <p class = "last-price">Precio: <span>$ 1,650.00</span></p>
          <p class = "new-price">Nuevo Precio: <span>$ 1,320.00 (20%)</span></p>
        </div>

        <div class = "product-detail">
          <h2>Detalles: </h2>
          <p>Detalles- Nivel de inglés con duración de 4 semanas efectivas de clase , con maestros en vivo  y con nuestro exclusivo método DAS way. </p>
          <p><b>Dirigido a :</b> Aspirantes a ingresar a UANL.</p>
          <ul>
            <li>Duración de 4 semanas</li>
            <li>Clases en vivo</li>
            <li>Material de clases incluido</li>
            <li>Acceso a la plataforma digital</li>
            <li>Acceso a grupo de WhatsApp con material exclusivo</li>
          </ul>
          <ul id="lista">
            <li style="background: url(includes/cssproduct/shoes_images/checked2.png) left center no-repeat;background-size: 18px;"><b>Curso EXCI con examen de simulación.</b></li>
            <li style="background: url(includes/cssproduct/shoes_images/checked2.png) left center no-repeat;background-size: 18px;"><b>Descuento 20%</b></li>
          </ul>
        </div>

        <div class = "purchase-info">
          <input type="" name="" id="cupon" placeholder="Ingresa el cupón">
          <!-- <button type = "button" class = "btn first" id="validar"> Validar   </button> -->
          <br/>
          <button type = "button" class = "btn" onclick="window.open('https://qa.fastenglish.com.mx/procesarpago.php')"> Comprar ahora   </button>
          <button type = "button" class = "btn third" onclick="window.open('https://api.whatsapp.com/send?phone=+528180124787&text=Quiero aplicar para la beca de CEAA2021')">Comprar más tarde</button>
        </div>

        <div style="font-size: 14px;">
          <a href="https://api.whatsapp.com/send?phone=+528180124787&text=Necesito información sobre el curso de inglés">Para cualquier duda sobre el curso, dar clic Aquí</a>
        </div>
      </div>
    </div>
  </div>

  
  <script>
    $('#promo').hide()
    $('#lista').hide()


    const validcupon = document.querySelector('#cupon');
    validcupon.addEventListener('keyup', (event) => {
      const cupon = document.querySelector('#cupon').value;
      if(cupon == 'CEAA2021'){
        $('#inicio').hide()
        $('#promo').show()
        $('#lista').show()
      }else{
        $('#promo').hide()
        $('#lista').hide()
        $('#inicio').show()
      }
    })
  </script>
</section>
