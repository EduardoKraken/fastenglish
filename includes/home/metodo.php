<div id="jewellery" class="Best"  style="padding-top: 40px;">
  <div class="container">
    <div class="row"> 
      <div class="col-md-12">
        <div class="titlepage">
          <h2>Método</h2>
        </div>
      </div>
    </div>
    <div class="row">

       <div class="col-md-12">

          <div class="best_main">
             <div class="row d_flex">
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 order-2 order-sm-2 order-md-1">
                   <div class="best_text">
                      <h2>Modalidades</h2>
                      <p>Cualquier lugar es bueno para trabajar en tu futuro. Puedes estudiar en cualquiera de nuestros <b>7 planteles</b> en Monterrey y su área metropolitana o bien puedes estudiar desde cualquier lugar del mundo con nuestras <b>clases en vivo</b>. No importa que modalidad elijas, en ambas modalidades se certificará tu nivel de inglés al finalizar el curso.</p>
                      <a href="https://api.whatsapp.com/send?phone=5218120494796 &text=Estoy en la página web, quiero información de la ubicación de los planteles." target="_blank">Información personalizada de los planteles</a>
                   </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 order-1 order-sm-1 order-md-2">
                   <div class="best_img">
                      <figure><img src="public/images/beimg.png"></figure>
                   </div>
                </div>
             </div>
          </div>

          <div class="best_main">
             <div class="row d_flex">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-sm-12">
                   <div class="best_img croos_rt">
                      <figure><img src="public/images/beimg2.png"></figure>
                   </div>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-sm-12">
                   <div class="best_text flot_left">
                      <h2>Horarios</h2>
                      <p>Sabemos que tienes más ocupaciones además de aprender inglés, así que no te preocupes. Damos clases los 7 días de la semana, así que solo es cuestión de que nos envíes un mensaje y encontraremos un horario que se adecue perfecto a tus necesidades.</p>
                      <a href="https://api.whatsapp.com/send?phone=5218120494796 &text=Estoy en la página web, quiero información sobre los horarios para estudiar inglés." target="_blank">Conocer horarios disponibles!</a>
                   </div>
                </div>
             </div>
          </div>

          <div class="best_main pa_bot">
             <div class="row d_flex">
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-sm-12 order-2 order-sm-2 order-md-1">
                   <div class="best_text">
                      <h2>Metodología</h2>
                      <p>¿Exámenes en papel? Eso quedo en el pasado, al ser parte de <b>FAST ENGLISH</b> podrás utilizar nuestra <b>App</b> cuantas veces quieras de manera <b>GRATUITA</b>, ahí podrás practicar tus lecciones, preguntar tus dudas y llevar el control absoluto de tu avance.</p>
                      <a href="https://api.whatsapp.com/send?phone=5218120494796 &text=Estoy en la página web y me gustaría conocer más sobre la Metodología" target="_blank">Más información</a>
                   </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-sm-12 order-1 order-sm-1 order-md-2">
                   <div class="best_img d_none">
                      <figure><img src="public/images/beimg3.png"></figure>
                   </div>
                </div>
             </div>
          </div>

          <div class="best_main">
             <div class="row d_flex">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-sm-12">
                   <div class="best_img croos_rt">
                      <figure><img src="public/images/beimg4.png"></figure>
                   </div>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-sm-12">
                   <div class="best_text flot_left">
                      <h2>Becas</h2>
                      <p>¡Tenemos un programa de becas para que aprendas al mejor precio, envíanos un WhatsApp y pide la tuya! Pide tu beca dando clic en el siguiente botón</p>
                      <a href="https://api.whatsapp.com/send?phone=5218120494796 &text=Estoy en la página web y me gustaría conocer las becas disponibles." target="_blank">Solicitar una beca!</a>
                   </div>
                </div>
             </div>
          </div>


          <div class="best_main pa_bot">
             <div class="row d_flex">
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-sm-12 order-2 order-sm-2 order-md-1">
                   <div class="best_text">
                      <h2>Examen de evaluación</h2>
                      <p>Inicia en el nivel adecuado para ti.<br/>
                        No todas las personas saben lo mismo, para asegurarnos que tu avance será el mejor puedes realizar nuestro examen de evaluación sin costo, asi conoceremos tus áreas de oportunidad y podremos asegurar que tu avance será a máxima velocidad.</p>
                      <a href="https://fastenglish.com.mx/english-level-test/" target="_blank">Examen</a>
                   </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-sm-12 order-1 order-sm-1 order-md-2">
                   <div class="best_img d_none">
                      <figure><img src="public/images/beimg5.png"></figure>
                   </div>
                </div>
             </div>
          </div>

       </div>
    </div>
  </div>
</div>

