<div id="jewellery" class="Best"  style="padding-top: 40px;">
  <div class="container">
    <div class="row"> 
      <div class="col-md-12">
        <div class="titlepage" style="padding-bottom: 0px !important;">
          <h2>EXCI</h2>
        </div>
      </div>
    </div>
    <div class="row">

     <div class="col-md-12">

      <div class="best_main">
       <div class="row d_flex">
        <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 order-2 order-sm-2 order-md-1">
         <div class="best_text">
          <h1>Curso para el examen EXCI de la UANL</h1>
          <p>
            En <b class="textoFuerte">Fast English</b> contamos con las mejores asesorías para acreditar el examen EXCI de la Universidad Autónoma de Nuevo León.
            <br/>
            <br/>
            Dentro de nuestro curso aprenderás la estructura del examen, te enseñaremos la teoría necesaria para acreditarlo y lo mejor de todo es que <b class="textoFuerte">nos enfocaremos en ejercicios prácticos</b> para que tengas la mejor preparación.
            <br/>
            <br/>
            Te brindaremos material adicional, como e-books y exámenes de simulación que te permitirán conocer a fondo cómo funciona el examen.
            <br/>
            <br/>
            Además, tendrás <b class="textoFuerte">acceso 24/7 a nuestra app</b> donde podrás practicar con ejercicios que utilizaran la misma estructura del EXCI.
            <br/>
            <br/>
            Si ya eres uno de nuestros alumnos, <b class="textoFuerte">podrás aprender inglés gratis</b>. Ya que este curso esta incluido solo por ser parte de la familia Fast English, y si aun no estudias con nosotros <b class="textoFuerte">envíanos un mensaje</b> y con gusto te daremos los detalles de las clases de inglés y las <b class="textoFuerte">promociones vigentes.</b>
            <br/>
            <br/>
            <b class="textoFuerte">Estas construyendo tu futuro, nosotros sabemos cómo ayudarte.</b>
          </p>
          <a href="https://api.whatsapp.com/send?phone=5218120494796 &text=Estoy en la página web, quiero información del curso EXCI." target="_blank">Quiero más información del curso EXCI</a>
        </div>
      </div>
      <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 order-1 order-sm-1 order-md-2">
       <div class="best_img">
        <figure><img src="public/images/beimg5.png"></figure>
      </div>
    </div>
  </div>
</div>
