<div  style="padding-top: 40px;">
  <div class="container">
    <div class="row justify-content-center d-flex flex-wrap align-items-center">
      <div class="col-xl-6 col-lg-8 col-md-6 mb-4">
        <h1 class="text-center text-primary" style="font-size: 32px;"><b>Realiza tu pago aquí</b></h1>
        <h5>Para realizar tu pago, necesitarás contar con tu matrícula, la cuál, podrás encontrar en tu <b>RECIBO DE PAGO</b>, o bien, deberás comunicarte con <b>sucursal</b> para que te la proporcionen</h5>

        <br/>

        <img src="public/images/pago.webp" class="img-fluid" />
        <div class="form-group text-center">
          <button class="btn btn-primary" href="https://qa.fastenglish.com.mx/procesarpago.php" onclick="window.open('https://qa.fastenglish.com.mx/procesarpago.php')">Realizar pago</button>
        </div>
      </div>
    </div>
  </div>
</div>
