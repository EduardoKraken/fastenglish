<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-md-6 mb-4 text-center">
                <h1>PROMOCIONES</h1>
            </div>
        </div>
        <div class="row p-5">
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <img src="public/images/pagina-web.jpg" class="mb-4" width="100%" alt="">
                <h5 class="mb-4">Hermanos</h5>
                <button class="btn btn-primary">APLICAR</button>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <img src="public/images/web-3.png" width="100%" class="mb-4" alt="">
                <h5 class="mb-4">$0 INSCRIPCIÓN</h5>
                <button class="btn btn-primary">APLICAR</button>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <img src="public/images/pagina-web-2.png" width="100%" class="mb-4" alt="">
                <h5 class="mb-4">20 % Descuento Primer Nivel</h5>
                <button class="btn btn-primary">APLICAR</button>
            </div>
        </div>
    </div>
</div>