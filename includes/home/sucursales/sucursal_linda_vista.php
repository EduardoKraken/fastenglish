<div class="container-fluid">

  <section class="welcome_section position-relative">
    <div class="container ">
      <div class="row">
        <div class="col-lg-6">
          <div class="welcome_container">
            <h1 class="text-primary"><b>Tenemos cursos de inglés para jóvenes y adultos .</b></h1>
            <h2>
              <p>
                Nuestra sucursal esta sobre avenida Linda Vista y cuenta con cursos para niveles básicos intermedios y avanzados.   
              </p>
            </h2>

            <h2>
              <p>
                Conoce nuestras instalaciones y comienza a aprender inglés de la mejor forma. No importa si estas en cero en el idioma o ya tienes un nivel intermedio o avanzado, nosotros <b class="textoFuerte">te ayudaremos a que logres tu meta</b> de ser bilingüe.
              </p>
            </h2>

            <h2>
              <p>
                Al ser parte de <b class="textoFuerte">Fast English</b> tendrás acceso a nuestras clases en el plantel y también a <b class="textoFuerte">herramientas digitales</b> para que puedas aprender inglés gratis desde tu celular, tablet o computadora.
              </p>
            </h2>

            <h2>
              <p>
                Será un gusto ayudarte, <b class="textoFuerte">contacta a nuestra escuela</b> y conoce todos los beneficios de nuestro curso. 
              </p>
            </h2>
          </div>
        </div>

        <div class="col-lg-6" align="center">
          <div class="welcome_container">
            <img src="public/images/sucursales/linda_vista.jpeg" class="img-fluid" style="max-height: 400px;">
          </div>
        </div>

      </div>
    </div>
  </section>

  <div class="container">
    <div class="row ">
      <div class="col-xl-8 col-md-6 mb-4">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3595.304241905932!2d-100.25665878461255!3d25.69434891765511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662eab3d61198b7%3A0x7a067fb45d8f9e1a!2sAv.%20Linda%20Vista%20209%2C%20Linda%20Vista%2C%2067130%20Guadalupe%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620411341146!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>
      <div class="col-xl-4 col-md-6 mb-4 text-center">
        <div class="col-xl-12 col-md-6">
          <h5><b>Datos de la sucursal</b></h5>
          <hr>
          <h5><i class="fa fa-map-marker-alt"></i> <b>Dirección:</b></h5>
          <p>Av. Linda Vista 209, Linda Vista, 67130 Guadalupe, N.L.</p>
          <hr>
          <h5><i class="fa fa-phone-alt"></i> <b>Teléfono:</b></h5>
          <p><a href="tel:8122331381"> Tel: (81) 2164 2470</a></p>
          <hr>
          <h5><i class="fa fa-clock"></i> <b>Horario:</b></h5>
          <p>Lunes a viernes: 09:00 a 21:00</p>
          <p>Sábados:         09:00 a 16:00</p>
          <p>Domingos:        09:00 a 15:00</p>
        </div>
        <div class="row text-center p-2">
          <a 
            href="https://api.whatsapp.com/send?phone=5218121642470&text=Estoy en la página web y me interesa información de la sucursal Linda Vista"
            target="_blank"
          >
            <button 
              type="button" 
              class="btn btn-success" 
            >
              Contactar a la sucursal Linda Vista
            </button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>