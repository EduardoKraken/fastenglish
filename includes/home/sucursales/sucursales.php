<div class="container-fluid">
  <div class="container">
    <div class="row mb-4">
      <div class="col-xl-12 col-md-6 mb-4 text-center mt-5">
        <h2 class="text-primary"><b><i class="fa fa-university"></i> Sucursal Apodaca</b></h2>
      </div>
    </div>
    <div class="row ">
      <div class="col-xl-8 col-md-6 mb-4">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d534.250414086413!2d-100.30384593598686!3d25.737709872294374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x866294f633dd3dfb%3A0x87a6e7307b53000f!2sINBI%20An%C3%A1huac%20-%20English%20School!5e0!3m2!1ses!2smx!4v1620167579770!5m2!1ses!2smx" width="100%" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>
      <div class="col-xl-4 col-md-6 mb-4 text-center">
        <div class="col-xl-12 col-md-6">
          <h5><b>Datos de la sucursal</b></h5>
          <hr>
          <h5><i class="fa fa-map-marker-alt"></i> <b>Dirección:</b></h5>
          <p>Calle José Santos Chocano 111, Anáhuac, 66450 San Nicolás de los Garza, N.L.</p>
          <hr>
          <h5><i class="fa fa-phone-alt"></i> <b>Teléfono:</b></h5>
          <p><a href="tel:8124735131"> Tel: 81-2473-5131</a></p>
          <hr>
          <h5><i class="fa fa-clock"></i> <b>Horario de atención</b></h5>
          <p>Lunes a Viernes de 8:30am a 9:00pm</p>
          <p>Sabados de 8:30am a 3:30pm</p>
        </div>
      </div>
    </div>
  </div>
</div>