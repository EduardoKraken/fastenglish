<div class="container-fluid pl-0 pr-0">
  </div>
    <img src="public/images/banner_arriba.webp" class="d-block w-100" alt="...">
  </div>


  <div class="container" style="--bs-gutter-x: 0rem; text-align: center; font-size: x-large;">
  	<h1>"Estás a un paso de ser BILINGÜE. Olvídate de la gramática aburrida y vamos a enseñarte inglés de una forma amena y lo mejor de todo a <b>MÁXIMA VELOCIDAD.</b>
  	 Tenemos los mejores cursos de inglés para jóvenes y adultos en Monterrey y su área metropolitana en la modalidad presencial y para todo México y Latinoamérica en nuestro curso en línea en vivo"</h1>
  	<br/>
  	<br/>
  	Mándanos un mensaje para informarte todo lo que tenemos para ti. Estás a punto de cumplir tu meta. <b>Nosotros estamos listos, ¿y tú?</b>"
  </div>

	<div id="containerCards" class="row pl-0 pr-0 ma-0" style="padding: 0px;--bs-gutter-x: 0rem; margin-top: 35px;">
		<div class="row text-center p-2" style="margin-top: 25px;">
			<h2>Cursos
				<span style="color: rgb(0, 8, 61); ">
					<b>IMPARTIDOS</b>
				</span>  
				<i class="fa fa-graduation-cap text-danger" aria-hidden="true"></i></h2>
				<br/>
				<span>Da clic en el curso que te interesa y conoce todos los detalles</span>
		</div>

    <div class="card">
    	<a href="metodo">
      	<img src="public/images/cursos/intensivo.webp" alt="intensivo">
      	<h2 style="margin-top: 6px;">Intensivo diario</h2>
    	</a>
    </div>
    
    <div class="card">
    	<a href="metodo">
      	<img src="public/images/cursos/sabatino.webp" alt="sabatino">
      	<h2 style="margin-top: 6px;">Intensivo sabatino</h2>
    	</a>
      <!-- <a href="#" class="text-danger">Leer más</a> -->
    </div>
    
    <div class="card">
    	<a href="metodo">
      	<img src="public/images/cursos/dominical.webp" alt="dominical">
      	<h2 style="margin-top: 6px;">Intensivo dominical</h2>
    	</a>
      <!-- <a href="#" class="text-danger">Leer más</a> -->
    </div>

  </div>
	

	<div class="container"  style="margin-top: 45px;">
		<div class="row text-center p-2" style="margin-top: 25px;">
			<h1>¿Por qué 
				<span style="color: rgb(0, 8, 61); ">
					<b>SOMOS la MEJOR</b>
				</span> 
				 escuela? <i class="fa fa-university text-danger" aria-hidden="true"></i>
			</h1>
		</div>

	  <div class="row pl-0 pr-0 ma-0 d-flex justify-content-around">

			<div class="col-xl-6 col-md-6 mb-4 align-items-center col-sm-12 order-2 order-sm-2 order-md-1">
				<ol id="lista4">
			    <li class="card">Método exclusivo DAS Way (Diviértete, Aprende y Socializa)</li>
			    <li class="card">Mayor contenido en menor tiempo</li>
			    <li class="card">Clases 100% en inglés</li>
			    <li class="card">Plataforma Virtual</li> 
			    <li class="card">Aulas Inteligentes</li> 
			    <li class="card">Certificación en base al Marco Común Europeo de Referencia. (MCER)</li>
			    <li class="card">Costo Único y sin cargos fantasmas</li> 
				</ol>
			</div>

			<div class="col-xl-4 col-md-6 mb-4 align-items-center text-center col-sm-12 order-1 order-sm-1 order-md-2" >
				<div class="best_main">
			    <div class="row d_flex">
		        <div class="best_img">
		          <figure><img src="public/images/mejor_escuela.webp" alt="" width="90%" height="auto" class="thumbnails"></figure>
		        </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>

	


	<div class="container">
		<div class="row text-center p-2" style=" margin-bottom: 25px;">
			<h1><i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i> Sucursales</h1>
			<br/>
		</div>

	  <div class="row  d-flex justify-content-around">
			<div class="col-xl-4 col-md-6 mb-4 align-items-center text-center" >
				<div class="best_main">
			    <div class="row d_flex">
		        <div class="best_img">
		          <figure><img src="public/images/sucursales.webp" alt="" width="90%" height="auto" class="thumbnails"></figure>
		        </div>
			    </div>
			  </div>
			</div>

			<div class="col-lg-6">
        <div class="row">
          <div class="col-md-6 mb-5">
            <div class="d-flex flex-column">
            	<div class="sucursal">
                <h2>Linda Vista</h2>
                <p class="mb-2">Tel: (81) 2164 2470</p>
                <a href="sucursal_linda_vista" >Clic aquí</a>
             	</div>
            </div>
          </div>

          <div class="col-md-6 mb-5">
            <div class="d-flex flex-column">
            	<div class="sucursal">
                <h2>Mitras</h2>
                <p class="mb-2">Tel: (81) 2261 4706 </p>
                <a href="sucursal_mitras" >Clic aquí</a>
             	</div>
            </div>
          </div>

          <div class="col-md-6 mb-5">
            <div class="d-flex flex-column">
            	<div class="sucursal">
                <h2>Apodaca</h2>
                <p class="mb-2">Tel: (81) 2168 2526 </p>
                <a href="sucursal_apodaca" >Clic aquí</a>
             	</div>
            </div>
          </div>

          <div class="col-md-6 mb-5">
            <div class="d-flex flex-column">
            	<div class="sucursal">
                <h2>Eloy Cavazos</h2>
                <p class="mb-2"> Tel: (81) 2556 9221</p>
                <a href="sucursal_eloy_cavazos" >Clic aquí</a>
             	</div>
            </div>
          </div>

          <div class="col-md-6 mb-5">
            <div class="d-flex flex-column">
            	<div class="sucursal">
                <h2>Rómulo Garza</h2>
                <p class="mb-2">Tel: (81) 8883 5200 </p>
                <a href="sucursal_romulo_garza" >Clic aquí</a>
             	</div>
            </div>
          </div>

          <div class="col-md-6 mb-5">
            <div class="d-flex flex-column">
            	<div class="sucursal">
                <h2>Online</h2>
                <p class="mb-2">Tel: (81) 2164 2470 </p>
                <a href="sucursal_online" >Clic aquí</a>
             	</div>
            </div>
          </div>

          <div class="col-md-6 mb-5">
            <div class="d-flex flex-column">
            	<div class="sucursal">
                <h2>Universidad</h2>
                <p class="mb-2"> Tel: (81) 2261 4358</p>
                <a href="sucursal_universidad" >Clic aquí</a>
             	</div>
            </div>
          </div>

          <div class="col-md-6 mb-5">
            <div class="d-flex flex-column">
            	<div class="sucursal">
                <h2>Noria</h2>
                <p class="mb-2"> Tel: (81) 1351 5613</p>
                <a href="sucursal_noria" >Clic aquí</a>
             	</div>
            </div>
          </div>

        </div>
		</div>
	</div>

	<div id="modalPromo" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel"> 
	  <div class="modal-dialog modal-dialog-centered modal-md"> 
		  <div class="modal-content">
		  	<div class="container" id="block">
	        <div class="modal-header">
		        <h1 class="modal-title justify-content-center d-flex flex-wrap align-items-center" id="exampleModalToggleLabel">Formulario de contacto</h1>
				    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      </div>

		      <div class="modal-body">
		      	<form>
				    	<h3 style="text-align: center; margin-bottom:10px;">Welcome to Fast English ⚡ Estás a un paso de ser parte de la familia FAST, escríbenos y en breve nos pondremos en contacto contigo para darte información de nuestros cursos y las promociones vigentes.</h3>

				    	<div class="form-group">     
		            <label for="nombre" class="colocar_nombre">Nombre
		              <span class="obligatorio">*</span>
		            </label>
		            <input  type="text" style="width: 100%;" name="introducir_nombre" id="nombre" required="obligatorio" placeholder="Escribe tu nombre">
		          </div>

		          <div class="form-group">  
		            <label for="email" class="colocar_email">Email
		              <span class="obligatorio"></span>
		            </label>
		            <input  style="width: 100%;" type="email" name="introducir_email" id="email" placeholder="Escribe tu Email">
		          </div>
	      		
	      			<div class="form-group">  
		            <label for="telefone" class="colocar_telefono">Teléfono
		            </label>
		            <input  style="width: 100%;" type="tel" name="introducir_telefono" id="telefono" placeholder="Escribe tu teléfono">
		          </div>
	        		
	        		<div class="form-group">  
		            <label for="mensaje" class="colocar_mensaje">Cuéntanos en que podemos ayudarte
		              <span class="obligatorio">*</span>
		            </label>   
		          	<textarea style="width: 100%;" name="introducir_mensaje" class="texto_mensaje" id="mensaje" required="obligatorio" placeholder="Deja aquí tu comentario..."></textarea> 
		          </div>

		          <button type="button" name="enviar_formulario" id="enviar" style="width: 100%;"><p>Enviar</p></button>

		          <span class="obligatorio"> * </span>los campos son obligatorios.
		        </form>        
		      </div>
	      </div>
		  </div>
	  </div>
	</div> 
</div>

<script>
	window.onload = function(){
		$('#modalPromo').modal('show');
	}

	abrirModalOne = function(){
		$('#modal1').modal('show');
	}

	abrirModalTwo = function(){
		$('#modal2').modal('show');
	}

	abrirModalTre = function(){
		$('#modal3').modal('show');
	}

	abrirModalFour = function(){
		$('#modal4').modal('show');
	}

	abrirModalFive = function(){
		$('#modal5').modal('show');
	}

	abrirModalSix = function(){
		$('#modal6').modal('show');
	}

	abrirModalSeven = function(){
		$('#modal7').modal('show');
	}

</script>

<style type="text/css">
	#block{
		background: white !important;
	}
</style>