<div class="container-fluid mb-4">
    <div class="row" style="background-size: cover; height: 61vh; background-image: url(public/images/banner_cursos.png); background-repeat: no-repeat;">
        <div class="container-sm d-flex flex-wrap align-items-center">
            <div class="col-xl-6 col-md-6 p-5">
                <h1>Un curso a tu medida!</h1>
                <h2>Contamos con un curso para ti , siempre tomando en cuenta tu disponibilidad , tu economia y el tiempo de aprendizaje.</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row text-center p-3">
            <h2>Nuestros Cursos</h2>
        </div>
        <div class="row p-3 shadow">
            <div class="col-xl-4 col-md-6 mb-4 d-flex flex-wrap align-items-center">
                <div class="card">
                    <img src="public/images/5-intensivo.png" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title">Intensivo Adultos</h5>
                        <p class="card-text">Este curso es para nuestros alumnos de 13 años en adelante que quieran aprender inglés en tiempo record.</p>
                        <a href="curso-intensivo-adultos" class="btn btn-primary float-end">Detalles</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 d-flex flex-wrap align-items-center">
                <div class="card">
                    <img src="public/images/2-SEMI-INTENSIVO.jpg" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title">Semi Intensivo Adultos</h5>
                        <p class="card-text">Este curso esta diseñado para todos nuestros alumnos que quieran aprender inglés pero no tienen mucho tiempo para clases.</p>
                        <a href="curso-semi-intensivo-adultos" class="btn btn-primary float-end">Detalles</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 d-flex flex-wrap align-items-center">
                <div class="card">
                    <img src="public/images/1-KIDS.jpg" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title">Intensivo Kids</h5>
                        <p class="card-text">Nuestro curso para los mas peques de la casa son a partir de los 8 años , la edad por excelencia para aprender un segundo idioma.</p>
                        <a href="curso-intensivo-kids" class="btn btn-primary float-end">Detalles</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 d-flex flex-wrap align-items-center">
                <div class="card">
                    <img src="public/images/3-TEENS.png" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title">Intensivo Teens</h5>
                        <p class="card-text">Este curso es dedicado a nuestros alumnos de 9 a 13 años , los cuales podremos complementar con la enseñanza del idioma de su educacion basica para un mejor resultado.</p>
                        <a href="curso-intensivo-kids" class="btn btn-primary float-end">Detalles</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 d-flex flex-wrap align-items-center">
                <div class="card">
                    <img src="public/images/4-EXCI.png" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title">EXCI</h5>
                        <p class="card-text">Este curso esta diseñado para todos nuestros alumnos que requieran acreditar el examen EXCI en la UANL , ya sea primer ingreso , graduación o intercambio.</p>
                        <a href="curso-exci" class="btn btn-primary float-end">Detalles</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 d-flex flex-wrap align-items-center">
                <div class="card">
                    <img src="public/images/6-toefl.png" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title">TOEFL</h5>
                        <p class="card-text">Este curso es la base para poder alcanzar los puntos necesarios para acreditar el examen TOEFL con reconocimiento mundial.</p>
                        <a href="#" class="btn btn-primary float-end">Detalles</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container p-5">
        <div class="row">
            <a href="examen-ubicacion" class="text-decoration-none">
                <div class="col-xl-12 col-md-6 mb-4 bg-primary text-center text-light p-5">
                    <h3>Realiza tu Examen de Ubicación</h3>
                    <p>Resultado Inmediato. Da click aquí.</p>
                </div>
            </a>
        </div>
    </div>
    <div class="row d-flex flex-wrap align-items-center" style="background-size: 100%; background-image: linear-gradient(180deg,#ffffff 0%,rgba(255,218,164,0.52) 100%),url(https://inbi.mx/wp-content/uploads/2020/12/language-school-illustration-13.png); background-color: #ffffff; height: 61vh; background-repeat: no-repeat">
        <div class="col-xl-12 col-md-6 mb-4 p-3 text-center">
            <h2>! Inscríbete hoy !</h2>
            <h6>No dejes pasar la oportunidad de ser parte de la mejor escuela y aprender inglés con el metodo sencillo y efectivo ! Inscribete hoy y recibe la promocion del mes.</h6>
            <a class="btn btn-danger mb-4" href="inscripcion">
                INSCRIBIRME <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </a>
            <a class="btn btn-success mb-4" href="examen-ubicacion">
                CONOCE TU NIVEL DE INGLES <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>