<div class="container pt-3 mb-4">
    <div class="row justify-content-center">
        <div class="col-xl-5 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <img class="mb-4" src="public/images/logo.webp" alt="" width="35%">
                                </div>
                                <form class="form-signin" action="" method="POST">
                                    <div class="alert alert-danger alert-dismissible fade" role="alert">
                                        Error
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="form-floating mb-4">
                                        <input type="text" class="form-control" id="usuario" placeholder="Usuario">
                                        <label for="floatingNombre">Usuario:</label>
                                    </div>
                                    <div class="form-floating mb-4">
                                        <input type="password" class="form-control" id="password" placeholder="Contraseña">
                                        <label for="floatingNombre">Contraseña:</label>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-primary btn-block btn-login" name="login" type="button" id="btn-iniciar-sesion">Iniciar Sesión</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>