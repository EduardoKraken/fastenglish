<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- FontAwesome JS-->
    <script defer src="includes/csscea/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="includes/csscea/css/theme.css" scoped>

</head> 

<section class="hero-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-5 mb-5 align-self-center">
        <div class="book-cover-holder">
          <img class="img-fluid book-cover" src="includes/csscea/images/devbook-cover.jpg" alt="book cover" >
          <!-- <div class="book-badge d-inline-block shadow" style="background: #C80000">
            20%<br>Descuento
          </div> -->
        </div><!--//book-cover-holder-->
      </div><!--col-->
      <div class="col-12 col-md-7 pt-5 mb-5 align-self-center">
        <div class="promo pe-md-3 pe-lg-5">
          <h1 class="headline mb-3">
            Nivel 1 - FAST ENGLISH 
          </h1><!--//headline-->
          <div class="subheadline mb-4">
            <!-- Descripción -->
            Curso de inglés
            
            <br>
          </div><!--//subheading-->

          <div class="subheadline mb-4">
            <!-- <b style="text-decoration:line-through; color: red;">$ 1,650.00</b><br/> -->
            <b style="font-size: 35px;">$ 1,320.00</b>
          </div><!--//subheading-->
            
          <div class="cta-holder row gx-md-3 gy-3 gy-md-0">
            <div class="col-12 col-md-auto">
              <a 
                class="btn btn-primary w-100" 
                ref="https://qa.fastenglish.com.mx/procesarpago.php"
                style="background:#020328"
              >
                PAGAR AHORA
              <a>
            </div>
          </div><!--//cta-holder-->
          
          <!-- <div class="hero-quotes mt-5">
            <div id="quotes-carousel" class="quotes-carousel carousel slide carousel-fade mb-5" data-bs-ride="carousel" data-bs-interval="8000">
              <ol class="carousel-indicators">
                <li data-bs-target="#quotes-carousel" data-bs-slide-to="0" class="active"></li>
                <li data-bs-target="#quotes-carousel" data-bs-slide-to="1"></li>
                <li data-bs-target="#quotes-carousel" data-bs-slide-to="2"></li>
              </ol>
              
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <blockquote class="quote p-4 theme-bg-light">
                    “Aprende de la mejor manera utilizando lo más nuevo en tecnología”    
                  </blockquote>
                </div>
                <div class="carousel-item">
                  <blockquote class="quote p-4 theme-bg-light">
                    “Los mejores teachers con las mejores prácticas de aprendizaje”
                  </blockquote>
                </div>
                <div class="carousel-item">
                  <blockquote class="quote p-4 theme-bg-light">
                    “Clases 100% online, material didáctico y más”
                  </blockquote>
                </div>
              </div>
            </div>
          </div> -->
        </div><!--//promo-->
      </div><!--col-->
    </div><!--//row-->
  </div><!--//container-->
</section><!--//hero-section-->