<section id="footer">
  <div class="container">
    <div class="row">
      <hr style="color: #fff">
    </div>  
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
        <ul class="list-unstyled list-inline social text-center">
          <li class="list-inline-item"><a href="https://es-la.facebook.com/fastenglishmty/" target="_blank" aria-label="Read more about Seminole tax hike"><i class="fab fa-facebook-f fa-2x"></i></a></li>
          <li class="list-inline-item"><a href="https://twitter.com/fastenglishmty?lang=es" target="_blank" aria-label="Read more about Seminole tax hike"><i class="fab fa-twitter fa-2x"></i></a></li>
          <li class="list-inline-item"><a href="https://www.instagram.com/fastenglishschool/?hl=es-la" target="_blank" aria-label="Read more about Seminole tax hike"><i class="fab fa-instagram fa-2x"></i></a></li>
        </ul>
      </div>
      <hr style="color: #fff">
    </div>  
  </div>
</section>
<a href="https://api.whatsapp.com/send?phone=5218120494796 &text=Estoy en la página web, quiero información de los cursos y promociones disponibles." class="float" target="_blank" id="boton_whatsapp" name="boton_whatsapp" aria-label="Read more about Seminole tax hike">
<i class="fab fa-whatsapp my-float"></i>
</a>
</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous">
</script>
<script src="public/js/all.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src='public/js/inicio.js'></script>

<!-- Archivos JS por pagina -->
<?php
switch ($titulo) {
    case 'Fast English':
      echo "<script src='public/js/inicio.js'></script>";
      break;

    case 'iniciar sesión':
      echo "<script src='public/js/iniciar_sesion.js'></script>";
      break;

    default:

    break;
}

?>



</html>