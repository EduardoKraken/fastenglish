<div class="sticky-top" style="background-color: rgb(0, 8, 61); color: #FFF">
  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
      <a class="navbar-brand mb-2" href="index.php" aria-label="Read more about Seminole tax hike">
        <img src="public/images/logo.webp" alt="" width="50">
      </a>

      <button class="navbar-toggler bg-white btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="width: 70px;">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent" style="text-align: right;">
        <ul class="navbar-nav  ms-auto" > 

          <li class="nav-item">
            <a class="nav-link m-2 <?php if ($titulo == 'inicio') { echo ' active'; } ?>" aria-current="page" href="index" style="color: #FFF">Inicio</a>
          </li>

          <li class="nav-item">
            <a class="nav-link m-2 <?php if ($titulo == 'metodo') { echo ' active'; } ?>" href="metodo" style="color: #FFF">Método</a>
          </li>

          <li class="nav-item">
            <a class="nav-link m-2 <?php if ($titulo == 'EXCI') { echo ' active'; } ?>" href="exci_menu" style="color: #FFF">EXCI</a>
          </li>

          <li class="nav-item dropdown" style="color: #FFF">
            <a class="nav-link m-2 dropdown-toggle 
              <?php 
                if ($titulo == 'Sucursales' || $titulo == 'Sucursal Linda vista' || $titulo == 'Sucursal Mitras' || $titulo == 'Sucursal Universidad' || $titulo == 'Sucursal Apodaca' || $titulo == 'Sucursal Noria' || $titulo == 'Sucursal Eloy Cavazos' || $titulo == 'Sucursal Rómulo Garza') { echo ' active'; } 
              ?>" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="color: #FFF">
              <i class="fa fa-map-marker-alt" aria-hidden="true" style="color: #FFF"></i> Sucursales
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li>
                <a class="dropdown-item <?php if ($titulo == 'Sucursal Apodaca') { echo 'active'; } ?>" href="sucursal_apodaca">Sucursal Apodaca</a>
              </li>
              <li>
                <a class="dropdown-item <?php if ($titulo == 'Sucursal Eloy Cavazos') { echo 'active'; } ?>" href="sucursal_eloy_cavazos">Sucursal Eloy Cavazos</a>
              </li>
              <li>
                <a class="dropdown-item <?php if ($titulo == 'Sucursal Linda vista') { echo 'active'; } ?>" href="sucursal_linda_vista">Sucursal Linda vista</a>
              </li>
              <li>
                <a class="dropdown-item <?php if ($titulo == 'Sucursal Mitras') { echo 'active'; } ?>" href="sucursal_mitras">Sucursal Mitras</a>
              </li>
              <li>
                <a class="dropdown-item <?php if ($titulo == 'Sucursal Noria') { echo 'active'; } ?>" href="sucursal_noria">Sucursal Noria</a>
              </li>
              <li>
                <a class="dropdown-item <?php if ($titulo == 'Sucursal Online') { echo 'active'; } ?>" href="sucursal_online">Sucursal Online</a>
              </li>
              <li>
                <a class="dropdown-item <?php if ($titulo == 'Sucursal Rómulo Garza') { echo 'active'; } ?>" href="sucursal_romulo_garza">Sucursal Rómulo Garza</a>
              </li>
              <li>
                <a class="dropdown-item <?php if ($titulo == 'Sucursal Universidad') { echo 'active'; } ?>" href="sucursal_universidad">Sucursal Universidad</a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a class="nav-link m-2" href="https://api.whatsapp.com/send?phone=5218120494796 &text=Estoy en la página web, quiero información de los cursos y promociones disponibles." target="_blank" style="color: #FFF">Contáctanos</a>
          </li>

          <li class="nav-item">
            <a class="nav-link m-2 " href="https://fastenglish.com.mx/english-level-test" style="color: #FFF">Examen de ubicación</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link m-2 " href="https://fastenglish.com.mx/lms/#/" style="color: #FFF" target="_blank">Iniciar Sesión</a>
          </li>
         
        </ul>
      </div>
    </div>
  </nav>
</div>