<?php session_start(); ?>
  <html lang="es" class="translated-ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Administración de registros.</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
      <div class="container">
          <div class="row">
                
              <div class="col-md-4"></div>
                
                  <div class="col-md-4">
                      <form class="form-signin" action="login.php" method="post">
                                      
                            <?php
                                if (isset($_POST['login'])){
                                  include('inc/conexion.php');

                                  $credencial    = mysqli_real_escape_string($conn, $_POST['credencial']);
                                  $pass          = mysqli_real_escape_string($conn, $_POST['inputPassword']);
                                  $pass          = md5($pass);

                                  $sql = "SELECT * FROM usuarios WHERE correo='".$credencial."' and pass='".$pass."'";
                                                    
                                  if (!$resultado = $conn->query($sql)) {
                                    $erroQuery = 1;  
                                  }

                                  $numRows = mysqli_num_rows($resultado);

                                  if ($numRows === 0) {
                                      $messageError = "No se pudo encontrar una coincidencia para los datos ingresados. Inténtelo de nuevo.";
                                  }else{
                                      $data          = $resultado->fetch_assoc();
                                      $matriz        = array();
                                      $matriz        = array(trim($data['id']),trim($data['correo']));
                                      $_SESSION["fast"] = $matriz;
                                      header("Location:consulta.php");
                                      $conn->close(); 
                                  }
                                }
                            ?>

                                                    
                                                    
                            <div class="text-center">
                            <img src="img/cropped-logo-fast-png-1.png">
                            <h1>Administración de registros. </h1> 
                            </div>


                            <h1 class="h3 mb-3 font-weight-normal">
                            <?php
                            if(isset($messageError) || !empty($messageError)){
                              echo $messageError;
                            }else{
                            ?>


                            </h1>
                            <p>
                            <font style="vertical-align: inherit;">Éste sistema es esclusivo de FastEnglish </font>
                            <code>:Derechos Reservados</code>
                            </p>
                            <?php
                            }
                            ?>
                                                  

                            <div class="form-label-group">
                            <input type="text" id="credencial" name="credencial" class="form-control" placeholder="" required="" autofocus="">
                            <label for="inputEmail">
                            <font style="vertical-align: inherit;">Usuario</font>
                            </label>
                            </div>

                            <div class="form-label-group">
                            <input type="password" id="inputPassword" name="inputPassword" class="form-control" required="" placeholder="">
                            <label for="inputPassword">
                            <font style="vertical-align: inherit;">Contraseña</font>
                            </label>
                            </div>


                            <button class="btn btn-lg btn-primary btn-block" name="login" type="submit">
                            <font style="vertical-align: inherit;">Entrar</font>
                            </button>

                            <p class="mt-5 mb-3 text-muted text-center">
                            <font style="vertical-align: inherit;">© FastEnglish</font>
                            </p>
                      </form>
                  </div>
                

              <div class="col-md-4"></div>

          </div>
                
      </div>

  </body>
</html>