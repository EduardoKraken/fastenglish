<?PHP
require_once 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');

/*
$tiempoInicia = date("Y-m-d H:i:s");
$hoy = date("Y-m-d");
$sqlEjecutado = "select id from control_envio_diario_emails where fecha_ejecucion = $hoy";
$regEjecucion = $conn->query($sqlEjecutado);

$numRegDia = $regEjecucion->num_rows;
if($numRegDia>0){
  
}else{
  
}
$nombre_envio = "ENVIO MASIVO LIGA PARA EXAMEN - CEAA";


$sqlInsert="INSERT INTO control_envio_diario_emails(
  nombre_envio,num_correos_enviados,fecha_ejecucion,fecha_inicio,fecha_termino,tiempo_ejecucion
) values ();";

*/




  $sqlEmails = "select uniq,nombre,correo from contactos_examenes where binEnvioMsg=0 and correo !='armando@gmail.com'";
  
  if (!$resultado = $conn->query($sqlEmails)) {
      echo "Error al obtener los emails";
      exit;
  }
  
  while($data = $resultado->fetch_assoc()){
    
    $uniq   = $data['uniq'];
    $nombre = $data['nombre'];
    $correo = $data['correo'];
  

            $to      = $correo;
            $subject = 'Codigo y enlace para exámen FASTENGLISH';
            $de      = 'FastEnglish@fastenglish.com.mx';

            $headers = "From: " . $de. "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            

            $message = "
            <!doctype html>
            <html lang=\"en\">
              <head>
                <title>Codigo y enlace para el examen FastEngish.</title>
            <style>
              body {
                margin: 0;
                padding: 0;
              }
              h1 {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 110%;
                font-weight: bold;
                color: #2E86C1;
              }

              h2 {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 100%;
                font-weight: bold;
                color: #2E86C1 ;
              }
            p{
              color: #666;
              font-family: Arial, Helvetica, sans-serif;
            }
              .center {
                position: relative;
                margin: auto;
                padding: auto;
                text-align: center;
              }
              .contenedor {
                width: 80%;
                height: auto;
                padding: 0;
                margin: auto;
                background-color: #fff;
              }
              .centro {
                width: 200px;
                height: 30px;
                vertical-align: middle;
              }
              .asado:active {
                border: none;
              }
              .linea {
                width: 100%;
                height: 1px;
                border: 1px solid #cdcdcd;
              }
              .go {
                background-color:#FF5733;
                color:#fff;
                border:none;
                padding:10px 15px 10px 15px;
                font-family: Arial, Helvetica, sans-serif;
              }
              .dato-title {
                font-size: 100%;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
              }
              .dato-description {
                font-size: 100%;
                font-weight: normal;
                font-family: Arial, Helvetica, sans-serif;
              }
              .negrita {
                font-size: 100%;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
              }
            </style>


            </head>
            <body>
              <div class=\"contenedor\">
                <h1>FASTENGLISH</h1>
                
                <h2>Titulo del correo.</h2>

                <p>
                Hola $nombre te informamos que el examen para practicar el apartado de inglés ya está activo en la liga que aparecerá al 
                final de este mensaje. Te recomendamos estudiar los conceptos vistos en tu clase de CEAA asesorías. Al finalizar el 
                examen se enviará a revisión y nos comunicaremos contigo a través del teléfono proporcionado para indicarte tu resultado. 
                Recuerda que habrá más exámenes para que sigas practicando y logres obtener la mayor cantidad de puntos de inglés en tu 
                examen de admisión. Mucho éxito.
                <br/><br/><br/>
                Importante: Favor de copiar y pegar el código que aparece a continuación. 
                No lo transcriba ya que podría cometer un error en alguna letra y evitar que su 
                resultado se genere de manera correcta.</p><br/><br/>
                Codigo: $uniq <br/><br/>
                Enlace: 
                <a href=\"https://www.fastenglish.com.mx/contacto-examen/examen-reforzamiento.php\"> Clic Aquí </a> <br/>
                <br/><br/><br/>



              </div>
            </body>
            </html>
            ";

              
              if(mail($to, $subject, $message, $headers)){
                $updateOk = "UPDATE contactos_examenes SET binEnvioMsg=1 where uniq='".$uniq."' and correo='".$correo."'";
                $conn->query($updateOk);
              }else{
                $updateNok = "UPDATE contactos_examenes SET binEnvioMsg=2 where uniq='".$uniq."' and correo='".$correo."'";
                $conn->query($updateNok);
              }
              
              sleep(5);
             
            
  }
  $conn->close(); 

  //$tiempoTermina =date("Y-m-d H:i:s");  
  //$intervalo = $tiempoInicia->diff($tiempoTermina);
  echo "Fin del script";
  
?>