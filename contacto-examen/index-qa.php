<?php
require_once 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <title>Plataforma de contacto Fast English</title>
  </head>
  <body>
    <?php
    if(!isset($_POST['entrar'])){
    ?>
      <div class="container"> <!--Inicia Container -->
      

        
        <!--Inicia el formulario -->
        <div class="row">
            
        <div class="col-md-2"></div>
            <div class="col-md-8">
            <div class="text-center">
            <img src="img/cropped-logo-fast-png-1.png">
            <h1>Registro para reforzamiento de Inglés. </h1> 
            </div>
                <form action="" method="POST" name="forma" id="forma">
                <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" required>
                      </div>

                      <div class="form-group">
                        <label for="correo">Correo</label>
                        <input type="email" class="form-control" name="correo" id="correo" required>
                      </div>

                      <div class="form-group">
                        <label for="telefono">WhatsApp</label>
                        <input type="text" class="form-control" name="telefono" id="telefono" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleFormControlSelect1">¿A que facultad desea ingresar?</label>
                        <select class="form-control" name="facultad" id="facultad" required>
                          <option>Seleccione una opción</option>
                          <option value="1">Agronomía</option>
                          <option value="2">Arquitectura</option>
                          <option value="3">Artes Escénicas</option>
                          <option value="4">Artes Visuales</option>
                          <option value="5">Ciencias Biológicas</option>

                          <option value="6">Ciencias de la Tierra</option>
                          <option value="7">Ciencias Físico - Matematicas</option>
                          <option value="8">Ciencias Forestales</option>
                          <option value="9">Ciencias Políticas y Relaciones Internacionales</option>
                          <option value="10">Ciencias Químicas</option>
                          <option value="11">Contaduría Pública y Administración</option>
                          <option value="12">Derecho y Criminología</option>
                          <option value="13">Economía</option>
                          <option value="14">Enfermería</option>

                          <option value="15">Filosofía y Letras</option>
                          <option value="16">Ingenieria Civil</option>
                          <option value="17">Ingeniería Mecánica y Eléctrica</option>
                          <option value="18">Medicina</option>
                          <option value="19">Medicina Veterinaria y Zootecnia</option>
                          <option value="20">Música</option>
                          <option value="21">Odontología</option>                          
                          <option value="22">Organización Deportiva</option>
                          <option value="23">Psicología</option>
                          <option value="24">Salud Pública y Nutrición</option>
                          <option value="25">Trabajo Social y Desarrollo Humano</option>
                        </select>
                        </div>
                        <?php


                            $sql = "SELECT * FROM sucursales";
                            if (!$resultado = $conn->query($sql)) {
                              echo "Error al buscar las su ursales";
                              exit;
                            }
                            $conn->close();  
                        ?>
                        <div class="form-group">
                        <label for="exampleFormControlSelect1">Sucursal</label>
                        <select class="form-control" name="sucursal" id="sucursal" required>
                          <option>Seleccione una opción</option>
                          <?php
                          while($data = $resultado->fetch_assoc()){
                            ?>
                            <option value="<?php echo $data['id'];?>"><?php echo $data['nombre'];?></option>
                            <?php
                          }
                          ?>
                          
                        </select>
                        </div>


                      
                  <div class="text-center">
                    <button type="submit" name="entrar" class="btn btn-primary">Solicitar Enlace</button>
                  </div>
                  </form>
                  <br/><br/>

            </div>
            <div class="col-md-2"></div>
        </div>
        <!-- Termina el formulario-->
        </div><!--Termima container-->
  <?php  
  }
  else
  {
          function limpiar($parametro){

          $parametro = str_replace('‘','',$parametro);
          $parametro = str_replace('"','',$parametro);
          $parametro = str_replace('–','',$parametro);
          $parametro = str_replace('/*','',$parametro);
          $parametro = str_replace('*/','',$parametro);
          $parametro = str_replace('xp_','',$parametro);
          $parametro = str_replace('<','',$parametro);
          $parametro = str_replace('>','',$parametro);
          $parametro = str_replace('insert','',$parametro);
          $parametro = str_replace('delete','',$parametro);
          $parametro = str_replace('update','',$parametro);
          $parametro = str_replace('select','',$parametro);
          $parametro = str_replace('drop','',$parametro);
          $parametro = str_replace('table','',$parametro);
          return $parametro;
          }

        $nombre          = limpiar($_POST['nombre']);
        $correo          = limpiar($_POST['correo']);
        $telefono        = limpiar($_POST['telefono']);
        $facultad        = limpiar($_POST['facultad']);
        $sucursal        = limpiar($_POST['sucursal']);

        $fecha           = date("Y-m-d H:i:s");
        $uniq      = uniqid();
        $origen="WEB";

        if (mysqli_connect_errno()) {
            echo "error al conectar la base de datos";
            exit();
        }

 
$sql = "INSERT INTO contactos_examenes (
  uniq,
  nombre,
  correo,telefono,facultad,sucursal,fecha_registro,origen) VALUES (
  '".$uniq."',
  '".$nombre."',
  '".$correo."',
  '".$telefono."',
  '".$facultad."',
    $sucursal,
  '".$fecha."',
  '".$origen."')";

if ($conn->query($sql) === TRUE) {
    
$to      = 'isc.gerardoflores@gmail.com,armando.molina@inbi.mx,fastenglishmty@gmail.com';
$subject = 'Registro de examen de reforzmiento FASTENGLISH.';
$headers = "From: " . $correo. "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$message = "
<!doctype html>
<html lang=\"en\">
  <head>
    <title>Solicitud de enlace de examen Fast English.</title>
<style>
  body {
    margin: 0;
    padding: 0;
  }
  h1 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 110%;
    font-weight: bold;
    color: #2E86C1;
  }

  h2 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 100%;
    font-weight: bold;
    color: #2E86C1 ;
  }
p{
  color: #666;
  font-family: Arial, Helvetica, sans-serif;
}
  .center {
    position: relative;
    margin: auto;
    padding: auto;
    text-align: center;
  }
  .contenedor {
    width: 80%;
    height: auto;
    padding: 0;
    margin: auto;
    background-color: #fff;
  }
  .centro {
    width: 200px;
    height: 30px;
    vertical-align: middle;
  }
  .asado:active {
    border: none;
  }
  .linea {
    width: 100%;
height: 1px;
border: 1px solid #cdcdcd;
  }
  .go {
    background-color:#FF5733;
    color:#fff;
    border:none;
    padding:10px 15px 10px 15px;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-title {
    font-size: 100%;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-description {
    font-size: 100%;
    font-weight: normal;
    font-family: Arial, Helvetica, sans-serif;
  }

</style>


</head>
<body>
  <div class=\"contenedor\">
    <h1>FASTENGLISH</h1>
    
    <h2>¡Solicitud de enlace para el examen de ubicacion de Fast English!</h2>

    <p>".$nombre." se a registrado en el formulario de contacto, est interesado en presentar el examen de reforzamiento de Inglés. </p>
    <br/><br/><br/>

      <table cellpadding=\"8\">
      <tr style=\'background: #eee;\'><td><span class=\"dato-title\">Name:</span> </td><td><span class=\"dato-description\">" . $nombre . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Correo:</span></td><td><span class=\"dato-description\">" . $correo . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Teléfono:</span> </td><td><span class=\"dato-description\">" . $telefono . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Facultad que desea ingresar:</span> </td><td><span class=\"dato-description\">" . $facultad . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Fecha:</span> </td><td><span class=\"dato-description\">" . $fecha . "</span></td></tr>
      </table>
      <br/><br/>


  </div>
</body>
</html>
";

  // Notificar a el personal de FastEnglish
  mail($to, $subject, $message, $headers); 

  // Notificar a el usuario que pidio informes
  $correoFast = 'informes@fastenglish.com.mx';
  $toUsuario   = $correo;
  $subject = "";
  $headers = "";
  $message = "";

$correoFast = 'FastEnglish@fastenglish.com.mx';
$toUsuario   = $correo;
$subject = 'Registro para reforzamiento de FASTENGLISH.';
$headers = "From: " . $correoFast . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$message = "
<!doctype html>
<html lang=\"en\">
  <head>
    <title>Solicitud de enlace para reforzamiento de Inglés FastEnglish.</title>
<style>
  body {
    margin: 0;
    padding: 0;
  }
  h1 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 110%;
    font-weight: bold;
    color: #2E86C1;
  }

  h2 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 100%;
    font-weight: bold;
    color: #2E86C1 ;
  }
p{
  color: #666;
  font-family: Arial, Helvetica, sans-serif;
}
  .center {
    position: relative;
    margin: auto;
    padding: auto;
    text-align: center;
  }
  .contenedor {
    width: 80%;
    height: auto;
    padding: 0;
    margin: auto;
    background-color: #fff;
  }
  .centro {
    width: 200px;
    height: 30px;
    vertical-align: middle;
  }
  .asado:active {
    border: none;
  }
  .linea {
    width: 100%;
height: 1px;
border: 1px solid #cdcdcd;
  }
  .go {
    background-color:#FF5733;
    color:#fff;
    border:none;
    padding:10px 15px 10px 15px;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-title {
    font-size: 100%;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-description {
    font-size: 100%;
    font-weight: normal;
    font-family: Arial, Helvetica, sans-serif;
  }

</style>


</head>
<body>
  <div class=\"contenedor\">
    <h1>FASTENGLISH</h1>
    

    <h2>¡Solicitud de enlace para el reforzamiento de Fast English!</h2>

    <p>Hola ".$nombre."! gracias por registrarte en la plataforma de reforzamiento de inglés en la cual estaremos revisando tú información
    sobre el examén, te estaremos enviando el enlace por este medio cuando esté activo tú examén. </p>
    <p>Para cualqueir duda puede contactarnos con un <a href=\"https://wa.link/alnn32\"> WhatsApp </a></p>
    <p>Saludos cordiales</p>
    <br/><br/><br/>


      <br/><br/>


  </div>
</body>
</html>
";
  mail($toUsuario, $subject, $message, $headers); 
  ?>
  <br/>
  <br/>
  <br/><br/>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
      </div>
      <div class="col-md-6">
      <div class="text-center">
        <img src="img/cropped-logo-fast-png-1.png">
      </div>
        <p class="text-center">Sus datos se han recibido, nos pondremos en contacto con usted para propocionarle el enlace de reforzamiento de Inglés.</p>
        <p class="text-center">Te enviamos un correo favor de revisar tu bandeja de entrada. </p>


      </div>
      <div class="col-md-3">
      </div>
    </div>
  </div>
  <script>
        setTimeout(function(){ window.location.href = "/index.php"; }, 3000);
        </script>
  <?php
  } else {
  echo "error al intentar registrar los datos";
  }
  $conn->close();
}
?>


 </body>
</html>