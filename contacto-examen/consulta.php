<?php
include('inc/session.php');
require_once 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <title>Plataforma de contacto Fast English</title>

  </head>
  <body>
  <div class="container"> <!--Inicia Container -->
        <div class="row">    
          <div class="col-md-2"></div>
          <div class="col-md-6 text-center"><img src="img/cropped-logo-fast-png-1.png" width="155" height="131"></div>
          <div class="col-md-4">
            <div> 
            Hola <?=$_SESSION["fast"][1];?> | <a href="inc/salir.php"> <i class="fa fa-sign-out" aria-hidden="true"></i> </a>
            </div>
          </div>
        </div>
  </div>


  <div class="container-fluid"> <!--Inicia Container -->
        <div class="row">    
          
          <div class="col-md-12">
          <?PHP
          $TAMANO_PAGINA = 10;
          $pagina = $_GET["pagina"];
          if (!$pagina) {
              $inicio = 0;
              $pagina=1;
          }else {
              $inicio = ($pagina - 1) * $TAMANO_PAGINA;
          }


          $ssql = "select * from respuestas_examen where revisado is null";
          $rs   = $conn->query($ssql);
          $numTotalRegistros = $rs->num_rows;
          $totalPaginas      = ceil($numTotalRegistros / $TAMANO_PAGINA);
          //pongo el número de registros total, el tamaño de página y la página que se muestra
          echo "Número de registros encontrados: <span class='badge badge-success'>" . $numTotalRegistros . "</span> <br>";
          echo "Se muestran páginas de " . $TAMANO_PAGINA . " registros cada una<br>";
          echo "Mostrando la página " . $pagina . " de " . $totalPaginas . "<p>";
          ?>
          <br/><br/>

          <?PHP
          //construyo la sentencia SQL
          //$ssql = "select * from pais " . $criterio . " limit " . $inicio . "," . $TAMANO_PAGINA;
          //$rs   = mysql_query($ssql);
          //while ($fila = mysql_fetch_object($rs)){
              //echo $fila->nombre_pais . "<br>";
          //}
          //mysqli_free_result($rs);
          //mysqli_close($conn);


            $sqlExamenes = "select id,nombre,correo,codigo,
            (select nombre from facultades where id=facultad) as nombreFacultad,
            carrera,fecha_registro,puntuacion_global from respuestas_examen where revisado is null limit " . $inicio . "," . $TAMANO_PAGINA;;

            if (!$resultado = $conn->query($sqlExamenes)) {
                echo "Error al obtener los emails";
                exit;
            }
            $numReg = $resultado->num_rows;
            ?>
  
            <br/>
            <style>
            table th {
              background-color:#CD5C5C;
              color:#fff;
              font-style:arial;
              font-size:90%;
            }
            .fecha {
              font-size:80%;
            }
            #registros tr:nth-child(even) {
            background-color: #eee;
            }
            #registros tr:nth-child(odd) {
            background-color: #fff;
            }
            input[type="button"] {
border: none;
outline:none;
}

            </style>
            <table id="registros">
            <tr>
            <th width="10"><span class="font-weight-bold"> Num Fila </span></th>
            <th width="10"><span class="font-weight-bold"> ID </span></th>
            <th width="20"><span class="font-weight-bold"> Nombre </span></th>
            <th width="20"><span class="font-weight-bold"> Correo </span></th>
            <th width="30"><span class="font-weight-bold"> Facultad </span></th>

            <th width="20"><span class="font-weight-bold"> Sucursal</span></th>
            <th width="20"><span class="font-weight-bold"> Telefono</span></th>
            
            

            <th width="20"><span class="font-weight-bold"> Carrera </span></th>
            <th width="10"><span class="font-weight-bold"> Puntuacion </span></th>
            <th width="20"><span class="font-weight-bold"> Fecha Registro </span></th>
            <th width="20"><span class="font-weight-bold"> Editar </span></th>
            </tr>
            <?php
            $i=0;
              while($data = $resultado->fetch_assoc()){
                $i=$i+1;
                $correo = $data['correo'];
                
                $selectS ="select sucursal from contactos_examenes where correo='".$correo."'";
                $idSuc = $conn->query($selectS);
                $idSucursal=$idSuc->fetch_assoc();
                
                $numRegSucursal = $idSuc->num_rows;
                if($numRegSucursal>0){
                  $idSS = $idSucursal['sucursal'];
                  $sqlSuc ="select nombre from sucursales where id=$idSS";
                  $nomSuc = $conn->query($sqlSuc);
                  $ns=$nomSuc->fetch_assoc();
                  $nombreSucursal = $ns['nombre'];
                }else{
                  $nombreSucursal = 'Sin Sucursal';
                }
                
                $sqlTel = "select telefono from contactos_examenes where correo='".$correo."'";
                $tel = $conn->query($sqlTel);
                $num=$tel->fetch_assoc();
                ?>
                <tr>
                <td><?=$i;?></td>
                <td><?=$data['id'];?></td>
                <td><?=$data['nombre'];?></td>
                <td><?=$data['correo'];?></td>
                <td><?=$data['nombreFacultad'];?></td>
                <td><?=$nombreSucursal;?></td>
                <td><?=$num['telefono'];?></td>
                <td><?=$data['carrera'];?></td>
                <td><?=$data['puntuacion_global'];?></td>
                <td class="fecha"><?=$data['fecha_registro'];?></td>
                <td class="fecha">
                  <button id="registrado<?=$data['id'];?>" value="<?=$data['id'];?>" onclick="registrado(this.value)">
                  <i class="fa fa-pencil-square-o" aria-hidden="true">
              </button></td>
                
                </tr>
                <?php
              
              }
            ?>
            </table>
            <br/><br/><br/>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <nav aria-label="Page navigation example">
            <ul class="pagination flex-wrap">

              <?PHP
              //muestro los distintos índices de las páginas, si es que hay varias páginas
              if ($totalPaginas > 1){
                for ($i=1;$i<=$totalPaginas;$i++){
                  if ($pagina == $i){
 
                      //si muestro el índice de la página actual, no coloco enlace
                      echo "<li class='page-item active'>";
                      echo "<a class='page-link' href=''> $pagina <span class='sr-only'>(current)</span></a>";
                      echo "</li>";
                  }else{

                      //si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página
                      echo "<li class='page-item'> <a class='page-link' href='consulta.php?pagina=" . $i . "'>" . $i . "</a></li>";
                  }
                }
              }
              ?>

            </ul>
            </nav>

      </div>
    </div>
  </div>

  <br/>
  <br/>
  <br/>
  
  <script src="js/jquery/jquery-3.3.1.min.js"></script>
  <script>
function registrado(valor){
  var editar = confirm("Actualizar el estatus a revisado");
  if (editar==true){
      $.ajax({
        type: "POST",
        url: "actualizar-registro.php",
        data: "identificador=" + valor,
        success : function(text){
            
        limpio = text.trim();
        if(limpio == "exito"){
          alert("Se modifico exitosamente");
        }else{
          alert("No se pudo modificar, consulte con el administrador del sistema");
        }      
      }
      });
  }


  setTimeout(function(){ window.location.href = "consulta-auxiliar.php"; }, 100);

/*
$.ajax({
    type: "POST",
    url: "actualizar-registro.php",
    data: "identificador=" + valor,
    success : function(text){
      
      limpio = text.trim();
      if(limpio == "exito"){
        alert("OK");
      }else{
        alert("NOK");
      }
      
    }
});*/

}



/*
$("#form-asignar").submit(function(event){
event.preventDefault();
mazinger();
});*/
</script>
</body>
</html>