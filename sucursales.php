<?php
  session_start();
  date_default_timezone_set('America/Monterrey');
  $titulo = "Sucursales";
  include 'includes/headers/header.php';
  include 'includes/menus/menu-superior.php';
  include 'includes/home/sucursales/sucursales.php';
  include 'includes/footers/footer.php';
?>