<?php
  session_start();
  date_default_timezone_set('America/Monterrey');
  $titulo = "Clases de inglés en línea para niños, jóvenes y adultos";
  $metadescription = "Aprovecha la facilidad de estudiar desde cualquier dispositivo electrónico. Aprende con nuestras clases en vivo y practica a tu ritmo. Conoce nuestros cursos.";
  
  include 'includes/headers/header.php';
  include 'includes/menus/menu-superior.php';
  include 'includes/home/sucursales/sucursal_online.php';
  include 'includes/footers/footer.php';
?>