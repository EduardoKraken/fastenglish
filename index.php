<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "Clases de inglés presenciales y en línea // Fast English";
    $metadescription = "Con nuestro método DAS WAY (Diviértete, aprende y socializa) aprenderás el idioma a máxima velocidad. Olvídate de clases aburridas y obsoletas. Tenemos becas.";
    
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/inicio.php';
    include 'includes/footers/footer.php';
?>