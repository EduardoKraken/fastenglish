<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../vendor/autoload.php';

require '../../vendor/phpmailer/phpmailer/src/Exception.php';
require '../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../../vendor/phpmailer/phpmailer/src/SMTP.php';

$nombre     = $_POST['nombre'];
$celular    = $_POST['celular'];
$mensaje    = $_POST['mensaje'];

$date = date('d-m-Y');
$mes = "";
list($day, $month, $year) = explode('-', $date);

if($month == '01'){
  $mes = "Enero";
}

if($month == '02'){
  $mes = "Febrero";
}

if($month == '03'){
  $mes = "Marzo";
}

if($month == '04'){
  $mes = "Abril";
}

if($month == '05'){
  $mes = "Mayo";
}

if($month == '06'){
  $mes = "Junio";
}

if($month == '07'){
  $mes = "Julio";
}

if($month == '08'){
  $mes = "Agosto";
}

if($month == '09'){
  $mes = "Septiembre";
}

if($month == '10'){
  $mes = "Octubre";
}

if($month == '11'){
  $mes = "Noviembre";
}

if($month == '12'){
  $mes = "Diciembre";
}

$fecha_final = $day . " de " . $mes . " del " . $year;


//Inicializa variable a devolver
$data = array();
try {
    $mail = new PHPMailer();
	// $mail->SMTPDebug = 3;
	// $mail = \Config\Services::email();
	// SMTP Parametros.
	$mail->isSMTP();
	// SMTP Server Host. 
	$mail->Host = 'smtp.gmail.com';
	// Use SMTP autentificacion. 
	$mail->SMTPAuth = true;
	// Introduce la incriptacion del sistema. 
	$mail->SMTPSecure = 'tls';
  // SMTP usuario. 
  $mail->Username = 'staff.inbischool@gmail.com';
  // SMTP contraseña. 
  $mail->Password = 'sgjywlyabjawvlzv';
  // Introduce el SMTP Puerto.
  $mail->Port = 587;
  // Activo condificacción utf-8
  $mail->CharSet = 'UTF-8';
  
  // Añade remitente.
  $mail->setFrom('web-master@inbi.mx', 'FAST ENGLISH - Sitio Web');

  // Añade destinatario.
  $mail->addAddress('informes@fastenglish.com.mx', 'Informes FAST ENGLISH');
  // Asunto del correo.
  $mail->Subject = 'Contacto Recibido en fastenglish.com.mx';
  
  // Cuerpo del correo.
  $mail->isHTML(TRUE);
  $mail->Body = 
  '<html>
    <h2>Nuevos datos de Contacto recibidos</h2>
    <p>Nombre: '. $nombre .'</p>
    <p>Celular: '. $celular .'</p>
    <p>Mensaje: '. $mensaje .'</p>
    <p>Fecha: '. $fecha_final .' </p>
  </html>';
  //$mail->AltBody = 'There is a great disturbance in the Force.';

  // En caso de incluir una imagen
  //$mail->addAttachment('/home/darth/star_wars.mp3', 'Star_Wars_music.mp3');

  // Enviar correo
  $mail->send();

  $data[] = array('error' => false, 'mensaje' => "Correo enviado");
  //retornar datos
  echo json_encode($data);
} catch (Exception $e) {
  $data[] = array('error' => true, 'mensaje' => $e->errorMessage());
  echo json_encode($data);
} catch (\Exception $e) {
  $data[] = array('error' => true, 'mensaje' => $e->getMessage());
  echo json_encode($data);
}
