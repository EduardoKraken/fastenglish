<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "El mejor curso de inglés //  Nadie te enseña más rápido ";
    $metadescription = "Olvídate dela gramática aburrida, nosotros te enseñaremos a que hables inglés tal cual hablas español. Conoce todos los detalles de la escuela y gánate una beca";
    
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/metodo.php';
    include 'includes/footers/footer.php';
?>