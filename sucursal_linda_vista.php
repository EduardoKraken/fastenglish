<?php
  session_start();
  date_default_timezone_set('America/Monterrey');
  $titulo = "Las mejores clases de inglés para jóvenes y adultos";
  $metadescription = "Te garantizamos que nadie te enseña inglés más rápido. Tenemos 7 escuelas en el área metropolitana, ser bilingüe está más cerca que nunca. Conoce más de Fast.";
  
  include 'includes/headers/header.php';
  include 'includes/menus/menu-superior.php';
  include 'includes/home/sucursales/sucursal_linda_vista.php';
  include 'includes/footers/footer.php';
?>