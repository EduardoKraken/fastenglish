<?php
  session_start();
  date_default_timezone_set('America/Monterrey');
  $titulo = "Curso para el examen EXCI de la UANL // GARANTIZADO";
  $metadescription = "En nuestro curso te garantizamos que incrementaremos tu puntaje en el examen. Podrás usar nuestro simulador del examen en la App 24/7. Conoce nuestros horarios.";

  include 'includes/headers/header.php';
  include 'includes/menus/menu-superior.php';
  include 'includes/home/exci/exci.php';
  include 'includes/footers/footer.php';
?>