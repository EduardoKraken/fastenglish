<?php
$name='BrushScriptMT';
$type='TTF';
$desc=array (
  'Ascent' => 889.0,
  'Descent' => -338.0,
  'CapHeight' => 889.0,
  'Flags' => 68,
  'FontBBox' => '[-208 -338 1006 917]',
  'ItalicAngle' => -37.0,
  'StemV' => 87.0,
  'MissingWidth' => 500.0,
);
$up=-100;
$ut=50;
$ttffile='C:/Windows/Fonts/BRUSHSCI.TTF';
$originalsize=53456;
$fontkey='brush script mt italic';
?>