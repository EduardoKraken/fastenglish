var modal_registro = "";

$(document).ready(function(){
  console.log('ui')
});

$('#enviar').click(function () {
  if ($('#nombre').val() == "") {
    $('#nombre').addClass('is-invalid');
  } else {
    $('#nombre').removeClass('is-invalid');
  }

  if ($('#telefono').val() == "") {
    $('#telefono').addClass('is-invalid');
  } else {
    $('#telefono').removeClass('is-invalid');
  }

  if ($('#nombre').val() == "" || $('#telefono').val() == "") {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Lo sentimos no pueden ir campos vacios',
    });

    return;
  }

  $.ajax({
    type: 'POST',
    url: "app/operaciones/contacto.php",
    data: {
      "nombre": $('#nombre').val(),
      "celular": $('#telefono').val(),
      "email": $('#email').val(),
      "mensaje": $('#mensaje').val()
    },
    dataType: 'JSON',
    beforeSend: function () {
      Swal.fire({
        icon: 'info',
        title: 'Un momento',
        text: 'Estamos enviando tus datos! Espera un momento por favor.',
        showCancelButton: false,
        showConfirmButton: false
      });
    },
  }).done(function (response) {
    if (response[0].error) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: response[0].mensaje,
      });
    } else {
      Swal.fire({
        icon: 'success',
        title: 'Excelente!',
        text: 'Tus datos han sido enviados con éxito, nos comunicaremos contigo lo mas pronto posible.',
      });
      $('#nombre').val('');
      $('#telefono').val('');
      $('#email').val('');
      $('#mensaje').val('');
      modal_registro.hide();
    }
  });
});