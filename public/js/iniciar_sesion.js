$(document).ready(function () {

});


$('#btn-iniciar-sesion').click(function () {
    $.ajax({
        type: 'POST',
        url: "app/operaciones/iniciar-sesion.php",
        data: {
            "usuario": $('#usuario').val(),
            "password": $('#password').val()
        },
        dataType: 'JSON',
    }).done(function (response) {
        if (response[0].error) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: response[0].mensaje,
            });
        } else {
            document.location.href = 'index.php';
        }
    });
});