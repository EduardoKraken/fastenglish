$('#btn-enviar-datos').click(function () {

  if ($('#floatingNombre').val() == "") {
    $('#floatingNombre').addClass('is-invalid');
  } else {
    $('#floatingNombre').removeClass('is-invalid');
  }

  if ($('#floatingCelular').val() == "") {
    $('#floatingCelular').addClass('is-invalid');
  } else {
    $('#floatingCelular').removeClass('is-invalid');
  }
	
  if ($('#floatingNombre').val() == "" || $('#floatingCelular').val() == "" ) {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Lo sentimos no pueden ir campos vacios',
    });

    return;
  }

  $.ajax({
    type: 'POST',
    url: "app/operaciones/contacto.php",
    data: {
      "nombre": $('#floatingNombre').val(),
      "celular": $('#floatingCelular').val(),
      "email": $('#floatingEmail').val(),
      "mensaje": $('#floatingMensaje').val()
    },
    dataType: 'JSON',
    beforeSend: function () {
      Swal.fire({
        icon: 'info',
        title: 'Un momento',
        text: 'Estamos enviando tus datos!',
        showCancelButton: false,
        showConfirmButton: false
      });
    },
  }).done(function (response) {
    if (response[0].error) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: response[0].mensaje,
      });
    } else {
      Swal.fire({
        icon: 'success',
        title: 'Excelente!',
        text: 'Tus datos han sido enviados con éxito, nos comunicaremos contigo lo mas pronto posible.',
      });
      $('#floatingNombre').val('');
      $('#floatingCelular').val('');
      $('#floatingEmail').val('');
      $('#floatingMensaje').val('');
    }
  });
});